import express from 'express';
import cors from 'cors';
import userRouter from './routes/user';
import groupRouter from './routes/group';
import userGroupRouter from './routes/userGroup';
import authRouter from './routes/auth';
import { connectToDb } from './database';
import { errorLogger } from './middlewaares/errorLogger';
import { httpLogger } from './middlewaares/httpLogger';
import checkToken from './middlewaares/checkToken';
import logger from './logger';

const app = express();
const port = 3000;

app.use(httpLogger);

process.on('unhandledRejection', (error, promise) => {
    logger.error(`'Unhandled promise rejection: ${error?.message}'`);
});

process.on('uncaughtException', (error, origin) => {
    logger.error(
        `Caught exception: ${error}\n` + `Exception origin: ${origin}`
    );
});

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(checkToken);
app.use(authRouter);
app.use(userRouter);
app.use(groupRouter);
app.use(userGroupRouter);

app.get('/', (req, res) => {
    res.send('Hello!');
});

app.use(errorLogger);

app.listen(port, async () => {
    logger.info(`server is listening on ${port}`);

    try {
        await connectToDb();
    } catch (error) {
        console.error(`Database connection is faild. Error: ${error.message}`);
    }
});
