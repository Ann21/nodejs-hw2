import { DataTypes, Optional, Model } from 'sequelize';
import db from '../database';

type Permissions = 'READ' | 'WRITE' | 'DELETE' | 'SHARE' | 'UPLOAD_FILES';
export interface GroupAttributes {
    id: string;
    name: string;
    permissions: Permissions;
}

interface GroupCreationAttributes extends Optional<GroupAttributes, 'id'> {}

interface GroupInstance
    extends Model<GroupAttributes, GroupCreationAttributes>,
        GroupAttributes {}

export const Group = db.define<GroupInstance>(
    'Group',
    {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        permissions: {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
    {
        timestamps: false
    }
);

export default class GroupModel {
    create = async (group) => {
        const createdGroup = await Group.create(group);

        return createdGroup;
    };

    getAll = async () => {
        const allGroups = await Group.findAll({
            raw: true
        });

        return allGroups;
    };

    getById = async (id) => {
        const group = await Group.findOne({
            where: { id }
        });

        return group;
    };

    remove = async (id) => {
        const group = await Group.destroy({
            where: { id }
        });

        return group;
    };

    update = async (id, body) => {
        const group = await Group.update(body, {
            where: { id }
        });

        return group;
    };
}
