import { DataTypes, Op, Optional, Model } from 'sequelize';
import bcrypt from 'bcrypt';
import db from '../database';
import { Group } from './group';

export interface UserAttributes {
    id: string;
    login: string;
    password: string;
    age: number;
    isDeleted: boolean;
}

interface UserCreationAttributes extends Optional<UserAttributes, 'id'> {}

interface UserInstance
    extends Model<UserAttributes, UserCreationAttributes>,
        UserAttributes {}

export const User = db.define<UserInstance>(
    'User',
    {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
        },
        login: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            set(value) {
                const salt = bcrypt.genSaltSync();
                this.setDataValue('password', bcrypt.hashSync(value, salt));
            }
        },
        age: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    },
    {
        timestamps: false
    }
);

User.belongsToMany(Group, { through: 'UserGroup' });
Group.belongsToMany(User, { through: 'UserGroup' });
export default class UserModel {
    create = async (user) => {
        const createdUser = await User.create(user);

        return createdUser;
    };

    getAll = async () => {
        const allUsers = await User.findAll({
            raw: true,
            where: { isDeleted: false }
        });

        return allUsers;
    };

    search = async (str, limit) => {
        const users = await User.findAll({
            where: {
                login: {
                    [Op.substring]: str
                },
                isDeleted: false
            },
            order: [['login', 'DESC']],
            limit
        });

        return users;
    };

    getById = async (id) => {
        const user = await User.findOne({
            where: { id, isDeleted: false }
        });

        return user;
    };

    getByLogin = async (login) => {
        const user = await User.findOne({
            where: { login, isDeleted: false }
        });

        return user;
    };

    remove = async (id) => {
        const user = await User.update(
            { isDeleted: true },
            { where: { id, isDeleted: false } }
        );

        return user;
    };

    update = async (id, body) => {
        const user = await User.update(body, {
            where: { id, isDeleted: false }
        });

        return user;
    };
}
