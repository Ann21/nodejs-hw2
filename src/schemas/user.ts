import * as Joi from 'joi';

export const createUserSchema = Joi.object({
    login: Joi.string().required(),
    age: Joi.number().required().integer().min(4).max(130),
    password: Joi.string()
        .regex(RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/))
        .required()
});

export const updateUserSchema = Joi.object({
    login: Joi.string(),
    age: Joi.number().integer().min(4).max(130),
    password: Joi.string().regex(
        RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/)
    )
});

export const paramUserSchema = Joi.object({
    id: Joi.string().required()
});
