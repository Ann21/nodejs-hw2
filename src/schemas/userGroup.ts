import * as Joi from 'joi';

export const addUsersToGroupSchema = Joi.object({
    users: Joi.array().required()
});

export const paramUsersToGroupSchema = Joi.object({
    groupId: Joi.string().required()
});
