import * as Joi from 'joi';

export const createGroupSchema = Joi.object({
    name: Joi.string().required(),
    permissions: Joi.string().required()
});

export const updateGroupSchema = Joi.object({
    name: Joi.string(),
    permissions: Joi.string()
});

export const paramGroupSchema = Joi.object({
    id: Joi.string().required()
});