import { Router } from 'express';
import {
    createUserSchema,
    updateUserSchema,
    paramUserSchema
} from '../schemas/user';
import { IUserRequest } from '../type.d';
import { createValidator } from 'express-joi-validation';
import {
    getUsers,
    getUserById,
    createUser,
    removeUser,
    updateUser
} from '../controllers/user';

const userRouter = Router();
const validator = createValidator();

userRouter.get('/users', async (req: IUserRequest, res, next) => {
    try {
        const users = await getUsers(req);
        return res.send(users);
    } catch (e) {
        next(e);
    }
});

userRouter.get(
    '/users/:id',
    validator.params(paramUserSchema),
    async (req: IUserRequest, res, next) => {
        try {
            const user = await getUserById(req);
            return user ? res.send(user) : res.status(404).send();
        } catch (e) {
            next(e);
        }
    }
);

userRouter.post(
    '/users',
    validator.body(createUserSchema),
    async (req: IUserRequest, res, next) => {
        if (!req.body) return res.sendStatus(400);

        try {
            const user = await createUser(req);
            return res.send(user);
        } catch (e) {
            next(e);
        }
    }
);

userRouter.delete(
    '/users/:id',
    validator.params(paramUserSchema),
    async (req: IUserRequest, res, next) => {
        try {
            const user = await removeUser(req);

            return user ? res.send(user) : res.status(404).send();
        } catch (e) {
            next(e);
        }
    }
);

userRouter.put(
    '/users/:id',
    validator.body(updateUserSchema),
    async (req: IUserRequest, res, next) => {
        try {
            if (!req.params.id) return res.sendStatus(400);
            const user = await updateUser(req);

            return user ? res.send(user) : res.status(404).send();
        } catch (e) {
            next(e);
        }
    }
);

export default userRouter;
