import { Router } from 'express';
import {
    createGroupSchema,
    updateGroupSchema,
    paramGroupSchema
} from '../schemas/group';
import { createValidator } from 'express-joi-validation';
import {
    getGroups,
    getGroupById,
    createGroup,
    removeGroup,
    updateGroup
} from '../controllers/group';

const groupRouter = Router();
const validator = createValidator();

groupRouter.get('/groups', async (req, res, next) => {
    try {
        const users = await getGroups();
        return res.send(users);
    } catch (e) {
        next(e);
    }
});

groupRouter.get(
    '/groups/:id',
    validator.params(paramGroupSchema),
    async (req, res, next) => {
        try {
            const user = await getGroupById(req);
            return user ? res.send(user) : res.status(404).send();
        } catch (e) {
            next(e);
        }
    }
);

groupRouter.post(
    '/groups',
    validator.body(createGroupSchema),
    async (req, res, next) => {
        if (!req.body) return res.sendStatus(400);

        try {
            const user = await createGroup(req);
            return res.send(user);
        } catch (e) {
            next(e);
        }
    }
);

groupRouter.delete(
    '/groups/:id',
    validator.params(paramGroupSchema),
    async (req, res, next) => {
        try {
            const user = await removeGroup(req);
            return user ? res.send(user) : res.status(404).send();
        } catch (e) {
            next(e);
        }
    }
);

groupRouter.put(
    '/groups/:id',
    validator.body(updateGroupSchema),
    async (req, res, next) => {
        if (!req.params.id) return res.sendStatus(400);

        try {
            const user = await updateGroup(req);
            return user ? res.send(user) : res.status(404).send();
        } catch (e) {
            next(e);
        }
    }
);

export default groupRouter;
