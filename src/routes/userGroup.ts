import { Router } from 'express';
import { createValidator } from 'express-joi-validation';
import {
    addUsersToGroupSchema,
    paramUsersToGroupSchema
} from '../schemas/userGroup';
import { addUsersToGroup } from '../controllers/userGroup';

const userGroupRouter = Router();
const validator = createValidator();

userGroupRouter.post(
    '/users/group/:groupId',
    validator.body(addUsersToGroupSchema),
    validator.params(paramUsersToGroupSchema),
    async (req, res) => {
        if (!req.body) return res.sendStatus(400);

        const user = await addUsersToGroup(req);
        return res.send(user);
    }
);

export default userGroupRouter;
