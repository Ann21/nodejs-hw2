import { Router } from 'express';
import { createValidator } from 'express-joi-validation';
import { authSchema } from '../schemas/auth';
import { auth } from '../controllers/auth';
import jwt from 'jsonwebtoken';
import { jwtSecret } from '../secrets';

const authRouter = Router();
const validator = createValidator();

authRouter.post('/auth', validator.body(authSchema), async (req, res, next) => {
    if (!req.body) return res.sendStatus(400);

    try {
        const user = await auth(req);

        const payload = {
            id: user.id,
            age: user.age,
            login: user.login
        };

        const accessToken = jwt.sign(payload, jwtSecret, {
            algorithm: 'HS256'
        });

        return res.json(accessToken);
    } catch (e) {
        next(e);
    }
});

export default authRouter;
