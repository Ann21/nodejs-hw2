create table users (
    id varchar(255) not null,
    login varchar(255) not null,
    password varchar(255) not null,
    age int not null,
    isDeleted boolean,
    primary key (id)
);

insert into users
    (id, login, password, age, isDeleted)
values 
    ('9b1deb4d-3b7d-4bad-9bdd-2c0d7b3dcb6d', 'Anna', 'Test1234', 31, false),
    ('1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed', 'Annie', 'Test1234', 26, false);