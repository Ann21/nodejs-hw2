import UserGroupService from '../services/userGroup';

export const addUsersToGroup = async (req) => {
    const groupId = req.params.groupId;
    const userIds = req.body.users;

    try {
        const group = await new UserGroupService().addUsersToGroup(
            groupId,
            userIds
        );

        return group;
    } catch (e) {
        throw e;
    }
};
