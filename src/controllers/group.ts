import GroupService from '../services/group';

export const getGroups = async () => {
    const groups = await new GroupService().getGroups();

    return groups;
};

export const getGroupById = async (req) => {
    const id = req.params.id;
    const group = await new GroupService().getById(id);

    return group;
};

export const createGroup = async (req) => {
    const group = await new GroupService().create(req.body);

    return group;
};

export const removeGroup = async (req) => {
    const id = req.params.id;
    // add try catch
    const group = await new GroupService().remove(id);

    return group;
};

export const updateGroup = async (req) => {
    const id = req.params.id;
    const body = req.body;
    const group = await new GroupService().update(id, body);

    return group;
};
