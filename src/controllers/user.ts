import UserService from '../services/user';

export const getUsers = async (req) => {
    const users = await new UserService().getUsers(req.query);

    return users;
};

export const getUserById = async (req) => {
    const id = req.params.id;
    const user = await new UserService().getById(id);

    return user;
};

export const createUser = async (req) => {
    const user = await new UserService().create(req.body);

    return user;
};

export const removeUser = async (req) => {
    const id = req.params.id;
    const user = await new UserService().remove(id);

    return user;
};

export const updateUser = async (req) => {
    const id = req.params.id;
    const body = req.body;
    const user = await new UserService().update(id, body);

    return user;
};
