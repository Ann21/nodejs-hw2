import AuthService from '../services/auth';

export const auth = async (req) => {
    const user = await new AuthService().getUserByLogin(
        req.body?.login,
        req.body?.password
    );

    return user;
};
