import logger from '../logger';

const getDurationInMilliseconds = (start) => {
    const NS_PER_SEC = 1e9;
    const NS_TO_MS = 1e6;
    const diff = process.hrtime(start);

    return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};

export const httpLogger = (req, res, next) => {
    logger.info(
        `Request Type:' ${req.method}, 'Request URL:', ${req.originalUrl}`
    );
    const start = process.hrtime();

    res.on('finish', () => {
        const durationInMilliseconds = getDurationInMilliseconds(start);
        logger.info(
            `${req.method} ${
                req.originalUrl
            } [execution time] ${durationInMilliseconds.toLocaleString()} ms`
        );
    });

    next();
};
