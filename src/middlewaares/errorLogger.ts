import AppError from '../errors/AppError';
import logger from '../logger';

export const errorLogger = (err, req, res, next) => {
    logger.error(
        `Error!!! ${err.status || 500} - ${req.originalUrl} - ${req.method}.
            ${
                err instanceof AppError
                    ? `${err.method}' in ${
                          err.service
                      } called with ${JSON.stringify(err.args)}: ${err.message}`
                    : ''
            }`
    );

    res.status(err.status || 500).send('Something broke!');
};
