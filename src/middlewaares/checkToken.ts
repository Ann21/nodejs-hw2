import jwt from 'jsonwebtoken';
import { jwtSecret } from '../secrets';
import AppError from '../errors/AppError';

const checkToken = (req, res, next) => {
    if (req.originalUrl === '/auth') return next();

    const authHeader = req.headers['authorization'];
    const token = authHeader?.split(' ')[1];

    if (!token) {
        throw new AppError('checkToken', '', 'No token provided.', '', 401);
    }

    return jwt.verify(token, jwtSecret, function (err, decoded) {
        if (err) {
            throw new AppError(
                'checkToken',
                '',
                'Failed to authenticate token.',
                '',
                403
            );
        }

        return next();
    });
};

export default checkToken;
