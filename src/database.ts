import { Sequelize } from 'sequelize';

const db = new Sequelize(
    'postgres://duylwatp:6B1uwewaZ4L9hSi-CtD4oh8FvK9F18AB@ella.db.elephantsql.com/duylwatp'
);

export const connectToDb = async () => {
    try {
        await db.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }

    await db.sync({});
};

export default db;
