import { Request } from 'express';

export interface User {
    id: string;
    login: string;
    password: string;
    age: number;
    isDeleted: boolean;
}

export interface IUserRequest extends Request {
    context: Record<string, Record<string, unknown>>;
}
