import { User } from '../models/user';
import { Group } from '../models/group';
import db from '../database';
import AppError from '../errors/AppError';

export default class UserGroupService {
    addUsersToGroup = async (groupId: string, userIds: string[]) => {
        try {
            let result = await db.transaction(async (transaction) => {
                const group = await Group.findByPk(groupId);
                await Promise.all(
                    userIds.map(async (id) => {
                        const user = await User.findByPk(id);
                        return user.addGroup(group, { transaction });
                    })
                );
            });

            return result;
        } catch (e) {
            throw new AppError(
                'addUsersToGroup',
                UserGroupService.name,
                e.message,
                groupId
            );
        }
    };
}
