import UserModel from '../models/user';
import AppError from '../errors/AppError';
import bcrypt from 'bcrypt';

export default class AuthService {
    getUserByLogin = async (login, password) => {
        try {
            const user = await new UserModel().getByLogin(login);

            if (!user.id || !bcrypt.compareSync(password, user.password)) {
                throw new AppError(
                    'getUserByLogin',
                    'Password is incorect',
                    '',
                    login,
                    404
                );
            } else {
                return user;
            }
        } catch (e) {
            throw new AppError(
                'getUserByLogin',
                AuthService.name,
                e.message,
                login,
                404
            );
        }
    };
}
