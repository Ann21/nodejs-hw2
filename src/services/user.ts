import UserModel from '../models/user';
import { v4 as uuidv4 } from 'uuid';
import AppError from '../errors/AppError';

export default class UserService {
    getUsers = async (query) => {
        const search = query.search;
        const limit: number = +query.limit;

        try {
            let users;
            if (search && limit) {
                users = await new UserModel().search(search, limit);
            } else {
                users = await new UserModel().getAll();
            }
            return users;
        } catch (e) {
            throw new AppError('getUsers', UserService.name, e.message);
        }
    };

    getById = async (id) => {
        try {
            const user = await new UserModel().getById(id);
            return user;
        } catch (e) {
            throw new AppError('getById', UserService.name, e.message, id);
        }
    };

    create = async (user) => {
        const id = uuidv4();
        const { login, age, password } = user;
        const newUser = {
            id,
            login,
            age,
            password,
            isDeleted: false
        };

        try {
            const createdUser = await new UserModel().create(newUser);
            return createdUser;
        } catch (e) {
            throw new AppError(
                'create',
                UserService.name,
                e.message,
                JSON.stringify(user)
            );
        }
    };

    remove = async (id) => {
        try {
            const user = await new UserModel().remove(id);
            return user;
        } catch (e) {
            throw new AppError('remove', UserService.name, e.message, id);
        }
    };

    update = async (id, body) => {
        try {
            const user = await new UserModel().update(id, body);
            return user;
        } catch (e) {
            throw new AppError('update', UserService.name, e.message, id);
        }
    };
}
