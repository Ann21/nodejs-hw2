import GroupModel from '../models/group';
import { v4 as uuidv4 } from 'uuid';
import AppError from '../errors/AppError';

export default class GroupService {
    getGroups = async () => {
        try {
            const groups = await new GroupModel().getAll();

            return groups;
        } catch (e) {
            throw new AppError('getGroups', GroupService.name, e.message);
        }
    };

    getById = async (id) => {
        try {
            const group = await new GroupModel().getById(id);
            return group;
        } catch (e) {
            throw new AppError('getById', GroupService.name, e.message, id);
        }
    };

    create = async (group) => {
        const id = uuidv4();
        const { name, permissions } = group;
        const newGroup = {
            name,
            permissions,
            id
        };

        try {
            const createdGroup = await new GroupModel().create(newGroup);
            return createdGroup;
        } catch (e) {
            throw new AppError(
                'create',
                GroupService.name,
                e.message,
                JSON.stringify(group)
            );
        }
    };

    remove = async (id) => {
        try {
            const group = await new GroupModel().remove(id);
            return group;
        } catch (e) {
            throw new AppError('remove', GroupService.name, e.message, id);
        }
    };

    update = async (id, body) => {
        try {
            const group = await new GroupModel().update(id, body);
            return group;
        } catch (e) {
            throw new AppError('update', GroupService.name, e.message, id);
        }
    };
}
