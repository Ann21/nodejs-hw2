export const up = (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Groups', [
        {
            id: '34ea0cba-ebd1-42c1-abcd-88a1936472ac',
            name: 'Mantee',
            permissions: 'WRITE'
        }
    ]);

    // add sync
};

export const down = (queryInterface, Sequelize) =>
    queryInterface.bulkDelete('Groups', null, {});
