export const up = (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [
        {
            id: '34ea0cba-ebd1-42c1-abcd-88a1936472ca',
            login: 'Ann',
            age: 31,
            password:
                '$2b$10$Ho1CNII5rEzdFiiLNjM4Keic4zTplRs/zpiWzbddnCImH279ol.vm',
            isDeleted: false
        }
    ]);
};

export const down = (queryInterface, Sequelize) =>
    queryInterface.bulkDelete('Users', null, {});
