export default class AppError extends Error {
    constructor(method, service, message?, args?, status?) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.method = method || '';

        this.message = message || 'Something went wrong. Please try again.';

        this.args = args || '';

        this.service = service || '';

        this.status = status || 500;
    }
}
